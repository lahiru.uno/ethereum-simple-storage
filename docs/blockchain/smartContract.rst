Simple Storage Smart Contract
=============================

In Ethereum, for Smart Contract development we use Solidity programming language.
This section explains the ``SimpleStorage`` smart contract code. In a Truffe project, all smart 
contracts will place in the ``contract`` directory. First, we have to create ``SimpleStorage.sol`` 
smart contract in ``contract`` directory. ::

	ethereum-simple-Storage
	|--contracts
	|  |--SimpleStorage.sol
	|--migrations
	|--test
	|--truffle-config.js

Then all the following code goes in it. 

Define License and solidity version
------------------------------------

First we define the licence identifier and the solidity version. 
Without defining the exact solidity version 
we can define a range of solidity versions like below. ::

    // SPDX-License-Identifier: MIT
    pragma solidity >=0.7.0 <0.9.0;



Define the contract with an Unsigned Integer value
---------------------------------------------------

After the version information, we define the contract and unsinged integer ``uint`` 
attribute to hold the storage value. We can use
``contract`` keyword and then the contract name 
``SimpleStorage`` to define our contract. 
We can use the ``constructor`` keyword to define the constructor of 
the smart contract and pass the constructor arguments. In this smart contract, 
we pass the initial value of the ``storedData`` attribute as a constructor argument. ::

    contract SimpleStorage {
        uint private storedData;
        
        constructor(uint _data) {
            storedData = _data;
        }
        ...
    }


Getter function to view the "storedData" value
----------------------------------------------

Then we define a getter function to view the usinged integer value of the ``storedData`` attribute we defined above.
The format of the view function as follows. ::

    function <function_name>() <visibility_of_the_function> view returns (<return_type>) {
        <function_body>
    }

We use the ``view`` keyword for getter functions and these functions will not change the smart contract state.

The getter function for read the ``storedData`` value in our smart contract as follows.:: 
    
    function get() public view returns (uint) {
        return storedData;
    }

Setter function to change the "storedData" value 
------------------------------------------------

Next, we define the setter function to change the ``storedData`` value. The structure of the setter function as follows. ::

    function <function_name>(<data_type> <data>) <visibility_of_the_function> {
        <function_body>
    }

The setter function for set the ``storedData`` value in our smart contract as follows.:: 
    
    function set(uint x) public {
        storedData = x;
    }

Complete Smart Contract
-----------------------

The final version of the smart contract for holding an unsigned integer value, 
the getter function to view the ``storedData`` value, 
and the setter function to set the ``storedData`` value as follows. ::

    // SPDX-License-Identifier: MIT
    pragma solidity >=0.7.0 <0.9.0;
    
    contract SimpleStorage {
        uint private storedData;
        
        constructor(uint _data) {
            storedData = _data;
        }

        function get() public view returns (uint) {
            return storedData;
        }

        function set(uint x) public {
            storedData = x;
        }
    }
