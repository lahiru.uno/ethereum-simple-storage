Initialize project with Truffle
===============================

We can run the following command in the root directory to initialize a truffle project. ::

   truffle init


Project structure
-----------------

Here is ``ethereum-simple-storage`` project structure when initialize using Truffle. ::

    ethereum-simple-Storage
    |--contracts
    |--migrations
    |--test
    |--truffle-config.js

.. note::
    Successful execution of above command will generate all the directories and files except ``client`` and 
    ``docs`` directories.
    We add the ``client`` directory when initialize the **React App**. 
    All the **Sphinx** documents were placed in the ``docs`` directory.