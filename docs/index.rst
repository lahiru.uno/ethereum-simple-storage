.. Ethereum Simple Storage documentation master file, created by
   sphinx-quickstart on Wed Jul  7 11:47:33 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Ethereum Simple Storage's documentation!
===================================================

This project is about building an Ethereum Decentralized Application(DApp) with **ReactJs**. 
Let's deploy a smart contract that holding a value
on a blockchain and try to view and modify it using React App.


.. toctree::
   :maxdepth: 2
   :caption: Introduction:

   introduction/applicationOverview

.. toctree::
   :maxdepth: 2
   :caption: Installation Guide:

   installation/guide
   installation/checkout
   installation/structure
   installation/deploySmartContract
   installation/metamask
   installation/reactApp
   installation/connectReactMetaMask

.. toctree::
   :maxdepth: 2
   :caption: Smart Contract Development:

   blockchain/init
   blockchain/smartContract
   blockchain/deploy

.. toctree::
   :maxdepth: 2
   :caption: React App Development:

   react/react
   react/app
