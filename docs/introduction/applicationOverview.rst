
Application Overview
=====================

Here is a preview of the Decentralized Application(DApp) that we will build in this project. 
We will be able to read and change the value of the smart contract deployed in our 
personal blockchain using the following React UI.

.. image:: ../images/app_overview.png

Architecture
------------

The following diagram shows the architecture of the project.

.. image:: ../images/architecture.png
