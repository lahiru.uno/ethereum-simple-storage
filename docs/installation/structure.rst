.. _project-structure-target:

Project structure
=================

Here is the folder structure of Ethereum Simple Storage project. ::

    ethereum-simple-Storage
    |--client
    |--contracts
    |--docs
    |--migrations
    |--test
    |--truffle-config.js


* ``client/``: React App directory.
* ``contracts/``: Directory for solidity smart contracts.
* ``docs/``: Directory for Sphinx documnets.
* ``migrations/``: Directory for scriptable deployment files.
* ``test/``: Directory for test files for test application and contracts.
* ``truffle-config.js``: Truffle configuration file.