Installation Guide
==================

The following subsections will describe how to install all the tools and dependencies
we need to set up and run this project. You can skip sections you already installed in your machine.

Install Git
--------------

Use the following command to install Homebrew in macOS. You can find more on Homebrew in
`here <https://brew.sh/>`_ ::

   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

Then you can install ``git`` on Mac using Homebrew. ::

   brew install git

This is one way of install git on Mac. You can find more methods 
in `here <https://git-scm.com/download/mac>`_.

You can select and download ``git`` for your Windows operating system 
from `this page <https://git-scm.com/downloads>`_. 

Install Node
---------------

You need to install Node Package Manager, or NPM, which comes with Node.js.  
You can see if you have Node already installed by going to your terminal and typing::

   node -v

If you don't have Node already installed you can visit the `Node.js website <https://nodejs.org/en/>`_ 
to download it. 
You can find both Mac and Windows versions of Node in there. 
It is recommended to use the LTS version of Node. 

Then install Node using the downloaded Node installer.

Install Truffle 
------------------

Truffle is a development environment, testing framework, 
and asset pipeline for blockchains using the Ethereum Virtual Machine (EVM). 
Truffle provides important features in smart contract development. In this project, 
we use Truffle for smart contract compilation and deploying them to the blockchain. 
You can find more on Truffle in 
`this site <https://www.trufflesuite.com/docs/truffle/overview>`_.

You can install Truffle globally in your machine using the following command::

   npm install -g truffle

This command can be used to install Truffle in both Mac and Windows.

Install Ganache.
-------------------

`Ganache <https://www.trufflesuite.com/ganache>`_ is a personal blockchain, which is a local 
development blockchain that can be used to mimic the behavior of a public blockchain. 
It can be used as your personal blockchain for Ethereum development. 
It is available on Windows, Mac, and Linux as a desktop application and a command line tool.

.. image:: ../images/ganache.png

You can find the latest release for your operating system 
`here <https://github.com/trufflesuite/ganache/releases>`_. 
Once you have downloaded the archived package, extract the installer and run through the setup steps. 
Once you've installed it, you should see following screen whenever you open it choose the **Quickstart** option:

.. image:: ../images/ganache_accounts.png

Install MetaMask
-------------------

Most major web browsers do not currently connect to blockchain networks, 
so we will have to install a browser extension that allows them to do this.
Metamask helps you to turn your web browser into a blockchain browser.

.. image:: ../images/metamask.png

To install Metamask, 
`visit this link <https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn?hl=en>`_ 
or search for the Metamask Chrome plugin in the Google Chrome web store. 
Once you have installed it, be sure that it had checked in your list of extensions.
You will see the fox icon on the top right-hand side of your Chrome browser when it is installed.

Metamask will also allow us to manage personal accounts when we connect to the blockchain, 
as well as manage Ether funds that we will need to pay for transactions.