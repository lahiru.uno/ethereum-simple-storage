Checking out the project
========================

After successfully install above required dependencies you can clone 
"Ethereum Simple Storage" project from GitLab using following command. ::

   git clone https://gitlab.com/gmc123/ethereum-simple-storage.git