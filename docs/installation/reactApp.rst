ReactApp
========

In this section, we are trying to run the **ReactApp**.
As described in the :ref:`project-structure-target` section ReactApp code is inside the ``client`` directory.
First, we install all dependencies for ReactApp and then we can run it.


Install ReactApp dependencies
-----------------------------

Open a new terminal in the ``client`` directory and execute all following commands.

All the **ReactApp** dependencies are configured in ``package.json`` file in ``client`` 
directory as follows.

.. image:: ../images/packageJson.png
    :width: 400

You can use the following command to install all dependencies. ::

   npm install


Run ReactApp
-------------

After successfully install all the dependencies run the following command to start the ReactApp. 
You can use the same terminal in the ``client`` directory which is used to run the above command. ::

   npm run start

This command will start the ReactApp on network port ``3000`` in your machine. 
Before running this command make sure your network port 
``3000`` is free to use. If any process already running on network port ``3000`` 
you can kill the process and run 
above command again.

If all commands execute successfully you can access the ReactApp from your browser.
Use Google Chrome browser and go to ``localhost:3000`` you can see the following UI.

.. image:: ../images/app_overview.png