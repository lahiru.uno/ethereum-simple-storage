Deploy Smart Contract to the Ganache
====================================

This section describes how to deploy your smart contract into the Ganache private Ethereum blockchain. 
First, you have to make sure you are running Ganache locally on your machine.
Then we can use Truffle to deploy smart contracts into the Ganache local blockchain.
The solidity version is already configured in the ``truffle-config.js`` file.

Run Ganache
-----------

Once you've installed Ganache, you should see the following screen whenever you open it and choose 
the **Quickstart** option:

.. image:: ../images/ganache_accounts.png

In this window, you can see 10 wallet accounts addresses with ``100ETH`` in each which can be used for testing purposes.
Also, you can find the ``MNEMONIC`` and ``RPC SERVER`` address which will be use later in this project.

Deploy Smart Contract using Truffle
-----------------------------------

We can execute the following command on the terminal in the project root directory to 
deploy the ``SimpleStorage`` smart-contract into the Ganache Etherum blockchain 
which will be running on network ``127.0.0.1`` port ``7545`` . 
Before executing the following command make sure that your Ganache Ethereum blockchain is 
running on your machine. ::

    truffle migrate --reset

we can use the ``--reset`` option to run all migrations from the beginning instead of running from the 
last complete migration.

.. _copy smart contract abi target:

Copy Smart Contract Build into the ReactApp
--------------------------------------------

After successfully execute the above command it will generate ``SimpleStorage.json`` 
file inside the ``./build/contracts/`` directory. 
Copy this ``SimpleStorage.json`` file in to ``./client/src/contracts/`` directory.
After this operation ``SimpleStorage.json`` file should be in two places inside the project as follows. ::

    ethereum-simple-Storage
    |--build
    |  |--contracts
    |  |  |--SimpleStorage.json
    |--client
    |  |--src
    |  |  |--contracts
    |  |  |  |--SimpleStorage.json
    |--contracts
    |--docs
    |--migrations
    |--test
    |--truffle-config.js

    