React Application
=================

We will develop a user interface to interact with ``SimpleStorage`` smart-contract using **React**.
We use **web3** to connect the React app with the smart contract and **Ant Design** 
to develop UI Components. 

Create ReactApp
----------------

we will execute the following command on the terminal in the project root directory. ::

    npx create-react-app client

This will generate the ``client`` directory in the project root directory and initializes a new ReactApp.

Install ReactApp Dependencies
-----------------------------

In a new terminal in the **client** directory, we can execute the following commands to install 
dependencies to React App.

To add **web3** dependency to ReactApp we can execute the following command. ::

    npm install web3

You can find more details about **web3** functions in 
`here <https://web3js.readthedocs.io/en/v1.3.4/index.html>`_.

To install **Ant Design** we can execute the following command. ::

    npm install antd

You can find more on **Ant Design** on `this site <https://ant.design/docs/react/introduce>`_ 
and more about the
**Ant Design Components** in `here <https://ant.design/components/overview/>`_.

When you install these dependencies into the ReactApp it will automatically add these dependencies to the ``package.json`` file in the ``client`` directory as follows.

.. image:: ../images/packageJson.png
    :width: 400

.. note::
    If you clone this **Ethereum Simple Storage** project from GitLab, all the ReactApp 
    dependencies were added to the ``package.json``. 
    You can execute the following command in a new terminal in the ``client`` directory to install ReactApp dependencies. ::

        npm install

