App.js
======

Web User Interface code goes in the ``App.js`` file in ``client`` directory. 
We define ``App`` as a funtional component in **React**. ::

    ethereum-simple-storage
    |--client
    |  |--src
    |  |  |--App.js

You can clear the ``App.js`` content and start inserting the following code into it.

Copy Smart Contract Build into the ReactApp
-------------------------------------------

As mentioned in the :ref:`copy smart contract abi target` section first copy the ``SimpleStorage.json`` json 
file from ``build/contracts`` directory to ``client/src/contracts`` directory after deploying the smart contracts 
into the blockchain. ::

    ethereum-simple-storage
    |--client
    |  |--src
    |  |  |--contracts
    |  |  |  |--SimpleStorage.json
    |  |  |--App.js

Import Dependencies
-------------------

In ``App.js`` first we import ``React`` and other modules from react packages. ::

    import React, { useEffect, useState } from 'react';

We can import **Ant Design** and its components using following code segment. ::

    import 'antd/dist/antd.css';
    import { Card, Form, InputNumber, Button, Layout, Row, Col, Divider } from 'antd';

Then we can import ``web3``. ::

    import Web3 from 'web3';

We can import smart contract build file from ``contracts`` directory as follows. ::

	import SimpleStorageArtifact from './contracts/SimpleStorage.json'

We refer this json build object as ``SimpleStorageArtifact`` in the ``App.js`` file. 
In next step we use the ``abi`` and the smart contract address from this json build file.

Initialize Web3 instance for Simple Storage Smart Contract
----------------------------------------------------------

As described in `here <https://web3js.readthedocs.io/en/v1.2.11/web3.html#>`_ 
"The Web3 class is an umbrella package to house all Ethereum related modules".
In following code segment we initialize ``Web3`` instance using ``web3`` provided by browser or else connecting to 
``127.0.0.1:7545``. Then we can use the smart contract address and ``abi`` 
from the ``SimpleStorageArtifact`` we imported before to initialize a **SimpleStorage** smart contract instance. 
We use this instance to interact with the smart contract later. ::

	const web3 = new Web3(Web3.givenProvider || 'http://127.0.0.1:7545');
	const simpleStorageAbi = SimpleStorageArtifact.abi;
	const simpleStorageAddress = SimpleStorageArtifact.networks[5777].address;
	const SimpleStorage = new web3.eth.Contract(simpleStorageAbi, simpleStorageAddress);

We can access the contract abi by ``SimpleStorageArtifact.abi``.
We can access the smart contract address by ``SimpleStorageArtifact.networks[5777].address``.
we use ``5777`` because we deploy our smart contract in tho the Ganache local blockchain. 
``5777`` is the Network ID of Ganache blockchain.
Different Etheruem networks has different Network IDs.
If we deploy our smart contract in to a different network we should use it's Network ID instead ``5777``.


ReactApp States
----------------

We maintain a state in App.js. In React, States are used to hold some information in a React component.
``useState`` hooks will be used to maintain states in ``App`` component. ::

    const [ currentValue, setCurrentValue ] = useState('');

``currentValue``: Holds the current value returns from **SimpleStorage** smart contract.

Get Current Value from Smart Contract
-------------------------------------

We can read the ``storedData`` value from the smart contract using our web3 ``SimpleStorage`` smart contract instance.
We can access methods in ``SimpleStorage`` smart contract with ``SimpleStorage.methods``. 
In the following code snippet, we call the ``get()`` method of the **SimpleStorage** smart contract.
Since this is a ``view`` function, we use ``call()`` method to call ``get()`` function. ::

    SimpleStorage.methods.get().call();

.. note ::
	View functions ensure that they will not modify the state. A function can be declared as view.

We define an asynchronus function to get the value from smart contract. When function returns the value from
smart contract return value will be set as the ``currentValue``. ::

    const getCurrentValue = async () => {
        // Get the value from the contract.
        const response = await SimpleStorage.methods.get().call();

        // Update current value with the result.
        setCurrentValue(response);
    }

UseEffect hook
--------------

In **React** we use ``useEffect`` hook to execute external calls from **ReactApp**.

We set the ``currentValue`` when application loaded. To achieve this we use ``useEffect`` hook.
We execute ``getCurrentValue`` function we defined above when application loaded as follows. ::

    useEffect(() => {
        getCurrentValue();
    }, [])

This ``useEffect`` method will be executed only after page loaded.

React Form
-----------

To display the ``currentValue`` getting from the smart contract and to change it 
we use a ``Form`` component from **Ant Design**.
``submitButtonHandler`` function will be called when form submitted. :: 

	<Form 
		...
		onFinish={submitButtonHandler}
	>
		<Form.Item label="Current value">
			<span>{currentValue}</span>
		</Form.Item>
		...
	</Form>

In this project, we use some **Ant Design Components** to build the form.
As shown in the above code snippet it will display the ``Current value`` fetched from the smart contract
at the top part of the form.

Later part of the form has a input field and a button to submit new value to the form. ::

	<Form ... >
		...
		<Form.Item label="Value" name="storageValue" rules={[{ required: true, message: 'Please enter value!' }]} >
			<InputNumber
				min="0"
				max="999999999999999"
				style={{ width: '100%' }}
				placeholder="Enter new value"
			/>
		</Form.Item>
		<Form.Item wrapperCol={{ span: 14, offset: 5 }}>
			<Button type="primary" htmlType="submit">Save Storage Value</Button>
		</Form.Item>
	</Form>

We define the number input field using the ``InputNumber`` component from **Ant Design**. 
Name of the this number input field is ``storageValue``.
We can access the input value of the number field by this name.
We will discuss more on the value submission in the next section.

Change Simple Storage Contract Value
------------------------------------

We can access the Ethereum wallet addresses using the following code. ::

    const accounts = await window.ethereum.enable();

As described in previous section we can access methods in SimpleStorage smart  contract using ``SimpleStorage.methods``.
Now we use ``set()`` method to set the ``storedData`` value in 
smart contract. ::

    SimpleStorage.methods.set(<new value>).send({ from: accounts[0] });

* ``set(values.storageValue)`` : we pass ``<new value>`` as a parameter for the ``set`` method as defined in the **SimpleStorage** smart contract. ``set`` method will change the smart contract state and it changes the ``storedData`` value in the blockchain.
* These types of methods will add new transactions to the blockchain. ``send(...)`` method will be used to call smart contract functions which will change the smart contract state. 
* ``{from: accounts[0]}`` : ``send`` method requires an object parameter which contains some transaction details. We need to specify the ``from`` account in the ``send`` method. We pass our active account in the browser which is handled using the MetaMask extension.

The final version of the ``submitButtonHandler`` function is as follows. 

.. code-block:: javascript

    const submitButtonHandler = async (values) => {
        const accounts = await window.ethereum.enable();
        // Stores input value in smart contract.
        await SimpleStorage.methods.set(values.storageValue).send({ from: accounts[0] });

        // Get the value from the contract to prove it worked.
        getCurrentValue();
    };

``submitButtonHandler`` takes ``values`` as a parameter.
This function will be triggered when user click the submit button of the form.(``Save Storage Value`` button).
``values`` object contains all fields values and we can access them using their name.
``values.storageValue`` will return the value of the ``InputNumber`` field in our form.
This value will be passed as the parameter to smart contract ``set`` method.

We use ``getCurrentValue`` function at the end of the ``submitButtonHandler`` function to load the new 
value from smart contract.


Complete App.js 
===============

Here is the complete version of the ``App.js`` script including the **Ant Design** components.

.. code-block:: javascript

	import React, { useEffect, useState } from 'react';
	import 'antd/dist/antd.css';
	import { Card, Form, InputNumber, Button, Layout, Row, Col, Divider } from 'antd';
	import Web3 from 'web3';
	import SimpleStorageArtifact from './contracts/SimpleStorage.json'

	function App() {
	  const [componentSize, setComponentSize] = useState('default');

	  const onFormLayoutChange = ({ size }) => {
	    setComponentSize(size);
	  };

	  const [ currentValue, setCurrentValue ] = useState('');

	  const web3 = new Web3(Web3.givenProvider || 'http://127.0.0.1:7545');
	  const simpleStorageAbi = SimpleStorageArtifact.abi;
	  const simpleStorageAddress = SimpleStorageArtifact.networks[5777].address;
	  const SimpleStorage = new web3.eth.Contract(simpleStorageAbi, simpleStorageAddress);

	  const getCurrentValue = async () => {
	    // Get the value from the contract.
	    const response = await SimpleStorage.methods.get().call();

	    // Update current value from the smart contract storage value.
	    setCurrentValue(response);
	  }

	  useEffect(() => {
	    getCurrentValue();
	  })

	  const submitButtonHandler = async (values) => {
	    const accounts = await window.ethereum.enable();
	    // Stores input value in smart contract.
	    await SimpleStorage.methods.set(values.storageValue).send({ from: accounts[0] });

	    // Get the value from the contract to prove it worked.
	    getCurrentValue();
	  };

	  return (
	    <Layout style={{ minHeight: '100vh', padding: '16px' }} >
	      <Row>
	        <Col xs={24} lg={15} xl={12} xxl={9}>
	          <Card title="Simple Storage" style={{ margin: '0px' }}>
	            <Form
	              labelCol={{
	                span: 5,
	              }}
	              wrapperCol={{
	                span: 16,
	              }}
	              layout="horizontal"
	              initialValues={{
	                size: componentSize,
	              }}
	              onValuesChange={onFormLayoutChange}
	              size={componentSize}
	              onFinish={submitButtonHandler}
	              labelAlign='left'
	            >
	              <Form.Item label="Current value">
	                <span>{currentValue}</span>
	              </Form.Item>
	              <Divider></Divider>

	              <Form.Item label="Value" name="storageValue" rules={[{ required: true, message: 'Please enter value!' }]} >
	                <InputNumber
	                  min="0"
	                  max="999999999999999"
	                  style={{ width: '100%' }}
	                  placeholder="Enter new value"
	                />
	              </Form.Item>
	              <Form.Item wrapperCol={{ span: 14, offset: 5 }}>
	                <Button type="primary" htmlType="submit">Save Storage Value</Button>
	              </Form.Item>
	            </Form>
	          </Card>
	        </Col>
	      </Row>
	    </Layout>
	  );
	}

	export default App;

